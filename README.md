# Karaoke Tech

El Karaoke Tech es un dispositivo que emula un conjunto de
botones que puede utilizarse para "calificar" los participantes
al estilo de ".. Got Talent". Una vez todos los botones sean
apretados al menos una vez la reproducción se detendrá para
que pueda cambiar el participante. Un LED se enciente y mantiene
encendido cada vez que alguien apreta un botón. 

A este proyecto lo acompaña un programa en Python que es
necesario ejecutar para que el control funcione adecuadamente.

Para funcionar es únicamente necesario abrir una playlist de
canciones en un dispositivo reproductor de audio o un playlist
de YouTube.

Este repositorio incluye las instrucciones de
instrucciones de armado del proyecto denominado 
"Basureros anti-perros" desarrollado como parte de los 
Laboratorios de Resiliencia Comunitarios.

Prototipo elaborado por: Mario Gómez @ Hackerspace San Salvador

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Resistencias de 330 Ohm         |        6 |
| LED Rojo                        |        6 |
| Botones                         |        6 |
| Jumper M/M                      |       13 |
| Cable USB                       |        1 |
| Breadboard                      |        1 |

**Nota:** Podría considerarse la elaboración de botones impresos
en 3D para convertir los pulsadores pequeños en botones pulsadores
grandes. Para este proyecto se requiere soldar cables a los botones
pulsadores para poder alejarlos de la breadboard.


## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_e4***.

