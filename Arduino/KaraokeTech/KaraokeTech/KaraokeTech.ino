const int boton1 = 2;
const int boton2 = 3;
const int boton3 = 4;
const int boton4 = 5;
const int boton5 = 6;
const int boton6 = 7;
const int led1 = 8;
const int led2 = 9;
const int led3 = 10;
const int led4 = 11;
const int led5 = 12;
const int led6 = 13;

void setup() {
  Serial.begin(9600);
  pinMode(boton1, INPUT_PULLUP);
  pinMode(boton2, INPUT_PULLUP);
  pinMode(boton3, INPUT_PULLUP);
  pinMode(boton4, INPUT_PULLUP);
  pinMode(boton5, INPUT_PULLUP);
  pinMode(boton6, INPUT_PULLUP);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(led5, OUTPUT);
  pinMode(led6, OUTPUT);
}

uint8_t disparador = 0;

void loop() {
  uint8_t valor_anterior = disparador;
  digitalWrite(led1, disparador & 1);
  digitalWrite(led1, disparador & (1<<1));
  digitalWrite(led1, disparador & (1<<2));
  digitalWrite(led1, disparador & (1<<3));
  digitalWrite(led1, disparador & (1<<4));
  digitalWrite(led1, disparador & (1<<5));
  
  if(disparador==0x3F) {
    if(
      digitalRead(boton1)==0 ||
      digitalRead(boton2)==0 ||
      digitalRead(boton3)==0 ||
      digitalRead(boton4)==0 ||
      digitalRead(boton5)==0 ||
      digitalRead(boton6)==0
      ) {
        disparador = 0;
        Serial.print('c'); // Envía señal de continuar
      }
  } else {
    if(digitalRead(boton1)==0) {
      disparador |= 1;
    }
    if(digitalRead(boton2)==0) {
      disparador |= 1<<1;
    }
    if(digitalRead(boton3)==0) {
      disparador |= 1<<2;
    }
    if(digitalRead(boton4)==0) {
      disparador |= 1<<3;
    }
    if(digitalRead(boton5)==0) {
      disparador |= 1<<4;
    }
    if(digitalRead(boton6)==0) {
      disparador |= 1<<5;
    }
    if( disparador == 0x3F &&
      valor_anterior != disparador)
      {
        Serial.print('s'); // Envia señal de paro
      }
  }
}
